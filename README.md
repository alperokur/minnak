# minnak

## Overview

This project is a C application that utilizes various libraries for functionality. It includes a debug build for development and a release build for production. The project is structured to facilitate easy building, cleaning, and Docker management.

## Directory Structure

```
.
├── build/                # Contains build artifacts
│   ├── debug/            # Debug build artifacts
│   └── release/          # Release build artifacts
└── src/                  # Source code
```

## Build Instructions

### Prerequisites

Ensure you have the following installed:
- GCC (GNU Compiler Collection)
- Make
- Required libraries:
  - libpq (PostgreSQL)
  - libcjson (C JSON library)
  - libmicrohttpd (HTTP server library)
  - argon2 (Password hashing function)
  - OpenSSL (for cryptographic functions)

### Building the Project

To build the project, run:

```bash
make
```

This will create the debug build by default. You can also build the release version.

### Debug Build

To build and run the debug version of the application, use:

```bash
make debug
```

### Release Build

To build and run the release version of the application, use:

```bash
make release
```

### Clean Build Artifacts

To clean up the build artifacts, run:

```bash
make clean
```

## Debugging

### Valgrind

To run the application with Valgrind for memory leak checking, use:

```bash
make valgrind
```

### GDB

To run the application with GDB for debugging, use:

```bash
make gdb
```

## Docker Management

### Start Docker Containers

To start the Docker containers defined in the `docker-compose.yml` file, run:

```bash
docker compose up -d
```

### Stop Docker Containers

To stop the running Docker containers, use:

```bash
docker compose down
```

## License

This project is licensed under the GNU General Public License (GPL) Version 3, 29 June 2007. See the LICENSE file for details.

## Acknowledgments

- [GCC](https://gcc.gnu.org/)
- [Valgrind](http://valgrind.org/)
- [GDB](https://www.gnu.org/software/gdb/)
- [Docker](https://www.docker.com/)
