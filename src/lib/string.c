#include "string.h"
#include <stdio.h>
#include <string.h>

void safe_strncpy(char *mutable_message, const char *message,
                  size_t message_length, size_t buffer_size) {
  if (message_length >= buffer_size) {
    fprintf(stderr, "Error: message length exceeds buffer size\n");
    return;
  }

  strncpy(mutable_message, message, message_length);

  mutable_message[message_length] = '\0';
}
