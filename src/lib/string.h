#ifndef STRING_H
#define STRING_H

#include <stdlib.h>

void safe_strncpy(char *mutable_message, const char *message,
                  size_t message_length, size_t buffer_size);

#endif // STRING_H
