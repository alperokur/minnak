#ifndef CONFIG_ENV_H
#define CONFIG_ENV_H

void load_env(const char *file_name);
char *env(const char *key);
void free_env();

#endif // CONFIG_ENV_H
