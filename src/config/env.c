#include "env.h"
#include "../util/log.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char *key;
  char *value;
} EnvVar;

EnvVar *env_vars = NULL;
int env_count = 0;

static char *trim_and_unquote(const char *str) {
  while (isspace((unsigned char)*str)) {
    str++;
  }

  const char *end = str + strlen(str) - 1;
  while (end > str && isspace((unsigned char)*end)) {
    end--;
  }

  size_t len = end - str + 1;
  char *result = (char *)malloc(len + 1);
  if (!result) {
    fprintf(stderr, "Memory allocation failed for trimmed string\n");
    return NULL;
  }

  memcpy(result, str, len);
  result[len] = '\0';

  if (len > 1 && result[0] == '"' && result[len - 1] == '"') {
    size_t new_len = len - 2;
    char *new_result = (char *)malloc(new_len + 1);
    if (!new_result) {
      fprintf(stderr, "Memory allocation failed for unquoted string\n");
      free(result);
      return NULL;
    }
    memcpy(new_result, result + 1, new_len);
    new_result[new_len] = '\0';
    free(result);
    return new_result;
  }

  return result;
}

static void process_line(char *line) {
  char *equals_sign = strchr(line, '=');
  if (!equals_sign) {
    LOG("No '=' found in line: '%s'", line);
    return;
  }

  *equals_sign = '\0';
  char *key = trim_and_unquote(line);
  char *value = trim_and_unquote(equals_sign + 1);

  if (key && value) {
    LOG("Found key-value pair: '%s'='%s'", key, value);
    env_vars = realloc(env_vars, (env_count + 1) * sizeof(EnvVar));
    if (!env_vars) {
      perror("Failed to allocate memory");
      free(key);
      free(value);
      return;
    }

    env_vars[env_count].key = key;
    env_vars[env_count].value = value;
    env_count++;
  } else {
    LOG("Failed to allocate memory for key or value");
    free(key);
    free(value);
  }
}

void load_env(const char *file_name) {
  LOG("Loading environment variables from file: '%s'", file_name);

  FILE *file = fopen(file_name, "r");
  if (!file) {
    perror("Failed to open .env file");
    return;
  }

  char line[256];
  while (fgets(line, sizeof(line), file)) {
    char *comment = strchr(line, '#');
    if (comment) {
      *comment = '\0';
    }

    if (strlen(line) == 0 || isspace((unsigned char)line[0])) {
      continue;
    }

    process_line(line);
  }

  fclose(file);
  LOG("Loaded %d environment variables", env_count);
}

char *env(const char *key) {
  for (int i = 0; i < env_count; i++) {
    if (strcmp(env_vars[i].key, key) == 0) {
      return env_vars[i].value;
    }
  }

  LOG("Key '%s' not found", key);
  return NULL;
}

void free_env() {
  LOG("Freeing environment variables");

  for (int i = 0; i < env_count; i++) {
    LOG("Freeing key-value pair: '%s'='%s'", env_vars[i].key,
        env_vars[i].value);
    free(env_vars[i].key);
    free(env_vars[i].value);
  }
  free(env_vars);
}
