#include "authentication.h"
#include "../config/env.h"
#include "../util/log.h"
#include <cjson/cJSON.h>
#include <openssl/buffer.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char *base64_encode(const unsigned char *input, int length) {
  LOG("Encoding data to base64, length: %d", length);

  BIO *bio = NULL;
  BIO *b64 = NULL;
  BUF_MEM *bufferPtr = NULL;

  b64 = BIO_new(BIO_f_base64());
  bio = BIO_new(BIO_s_mem());
  bio = BIO_push(b64, bio);
  BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

  BIO_write(bio, input, length);
  BIO_flush(bio);
  BIO_get_mem_ptr(bio, &bufferPtr);

  char *encoded_data = strndup(bufferPtr->data, bufferPtr->length);
  BIO_free_all(bio);

  LOG("Base64 encoded data: %s", encoded_data);
  return encoded_data;
}

static char *base64_decode(const char *input, int length, int *out_len) {
  LOG("Decoding base64 data, length: %d", length);

  BIO *bio = NULL;
  BIO *b64 = NULL;
  char *buffer = (char *)malloc(length);

  memset(buffer, 0, length);
  bio = BIO_new_mem_buf(input, length);
  b64 = BIO_new(BIO_f_base64());
  bio = BIO_push(b64, bio);
  BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

  *out_len = BIO_read(bio, buffer, length);
  BIO_free_all(bio);

  LOG("Base64 decoded data length: %d", *out_len);
  return buffer;
}

unsigned char *hmac_sha256(const char *key, const char *data) {
  if (key == NULL || data == NULL) {
    fprintf(stderr, "Key or data is NULL\n");
    return NULL;
  }
  LOG("Generating HMAC SHA256");
  static unsigned char result[32];
  unsigned int len = sizeof(result);
  HMAC(EVP_sha256(), key, (int)strlen(key), (const unsigned char *)data,
       strlen(data), result, &len);
  return result;
}

char *create_jwt(const int id, const char *username) {
  LOG("Creating JWT for user: %s with id: %d", username, id);
  const char *secret = env("JWT_SECRET");
  const char *expiresInStr = env("JWT_EXPIRE");
  if (secret == NULL || expiresInStr == NULL) {
    LOG("JWT secret or expiration time not set");
    return NULL;
  }
  int expiresIn = atoi(expiresInStr);
  cJSON *header = cJSON_CreateObject();
  cJSON_AddStringToObject(header, "alg", "HS256");
  cJSON_AddStringToObject(header, "typ", "JWT");
  char *header_json = cJSON_PrintUnformatted(header);
  if (header_json == NULL) {
    LOG("Failed to create header JSON");
    cJSON_Delete(header);
    return NULL;
  }
  char *header_encoded = base64_encode((const unsigned char *)header_json,
                                       (int)strlen(header_json));
  cJSON *payload = cJSON_CreateObject();
  cJSON_AddNumberToObject(payload, "id", id);
  cJSON_AddStringToObject(payload, "username", username);
  cJSON_AddNumberToObject(payload, "iat", (double)time(NULL));
  cJSON_AddNumberToObject(payload, "exp", (double)(time(NULL) + expiresIn));
  char *payload_json = cJSON_PrintUnformatted(payload);
  char *payload_encoded = base64_encode((const unsigned char *)payload_json,
                                        (int)strlen(payload_json));
  if (payload_json == NULL) {
    LOG("Failed to create payload JSON");
    free(header_json);
    cJSON_Delete(header);
    cJSON_Delete(payload);
    return NULL;
  }
  char *signature_input =
      (char *)malloc(strlen(header_encoded) + strlen(payload_encoded) + 2);
  sprintf(signature_input, "%s.%s", header_encoded, payload_encoded);
  const unsigned char *hmac_result = hmac_sha256(secret, signature_input);
  char *signature_encoded = base64_encode(hmac_result, 32);
  char *jwt = (char *)malloc(strlen(header_encoded) + strlen(payload_encoded) +
                             strlen(signature_encoded) + 3);
  sprintf(jwt, "%s.%s.%s", header_encoded, payload_encoded, signature_encoded);

  LOG("Generated JWT: %s", jwt);

  free(signature_encoded);
  free(header_json);
  free(payload_json);
  free(header_encoded);
  free(payload_encoded);
  free(signature_input);
  cJSON_Delete(header);
  cJSON_Delete(payload);
  return jwt;
}

char *parse_jwt_payload(const char *jwt) {
  LOG("Parsing JWT payload");
  char *token_copy = strdup(jwt);
  const char *header_encoded;
  const char *payload_encoded;
  const char *signature_encoded;
  char *saveptr;

  header_encoded = strtok_r(token_copy, ".", &saveptr);
  payload_encoded = strtok_r(NULL, ".", &saveptr);
  signature_encoded = strtok_r(NULL, ".", &saveptr);

  if (header_encoded == NULL || payload_encoded == NULL ||
      signature_encoded == NULL) {
    LOG("Invalid JWT format");
    free(token_copy);
    return NULL;
  }

  int payload_len;
  char *payload_decoded = base64_decode(
      payload_encoded, (int)strlen(payload_encoded), &payload_len);

  if (payload_decoded == NULL) {
    LOG("Failed to decode payload");
    free(token_copy);
    return NULL;
  }

  char *payload_json = strndup(payload_decoded, payload_len);
  free(payload_decoded);
  free(token_copy);

  if (payload_json == NULL) {
    LOG("Failed to duplicate payload JSON");
    return NULL;
  }

  LOG("Parsed payload: %s", payload_json);
  return payload_json;
}

int is_jwt_expired(const char *jwt) {
  LOG("Checking if JWT is expired");
  char *payload_json = parse_jwt_payload(jwt);
  if (payload_json == NULL) {
    LOG("Failed to parse JWT payload");
    return 0;
  }
  cJSON *payload = cJSON_Parse(payload_json);
  if (payload == NULL) {
    LOG("Failed to parse JSON from payload");
    free(payload_json);
    return 0;
  }
  const cJSON *exp_item = cJSON_GetObjectItem(payload, "exp");
  if (!cJSON_IsNumber(exp_item)) {
    LOG("Expiration time is not a number");
    cJSON_Delete(payload);
    free(payload_json);
    return 0;
  }
  time_t current_time = time(NULL);
  time_t expiration_time = (time_t)exp_item->valuedouble;
  cJSON_Delete(payload);
  free(payload_json);
  LOG("Current time: %ld, Expiration time: %ld", current_time, expiration_time);
  return (expiration_time >= current_time) ? 1 : 0;
}

int validate_jwt(const char *jwt) {
  LOG("Validating JWT");
  const char *secret = env("JWT_SECRET");
  if (secret == NULL) {
    LOG("JWT secret not set");
    return 0;
  }

  char *token_copy = strdup(jwt);
  char *header_encoded;
  char *payload_encoded;
  const char *signature_encoded;
  char *saveptr;

  header_encoded = strtok_r(token_copy, ".", &saveptr);
  payload_encoded = strtok_r(NULL, ".", &saveptr);
  signature_encoded = strtok_r(NULL, ".", &saveptr);

  if (header_encoded == NULL || payload_encoded == NULL ||
      signature_encoded == NULL) {
    LOG("Invalid JWT format");
    free(token_copy);
    return 0;
  }

  char *signature_input =
      (char *)malloc(strlen(header_encoded) + strlen(payload_encoded) + 2);
  sprintf(signature_input, "%s.%s", header_encoded, payload_encoded);
  const unsigned char *hmac_result = hmac_sha256(secret, signature_input);
  char *signature_encoded_computed = base64_encode(hmac_result, 32);
  int is_valid = (strcmp(signature_encoded_computed, signature_encoded) == 0) &&
                 is_jwt_expired(jwt);

  LOG("JWT validation result: %s", is_valid ? "valid" : "invalid");

  free(signature_input);
  free(signature_encoded_computed);
  free(token_copy);
  return is_valid ? 1 : 0;
}

int test_jwt() {
  const int id = 1;
  const char *username = "admin";
  char *jwt = create_jwt(id, username);
  printf("JWT: %s\n", jwt);
  free(jwt);
  return 0;
}
