#include "encryption.h"
#include "../util/log.h"
#include "argon2.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASHLEN 32
#define SALTLEN 16
#define T_COST 2
#define M_COST (1 << 16)
#define PARALLELISM 1

void generate_salt(uint8_t *salt, size_t length) {
  LOG("Generating salt of length: %zu", length);
  for (size_t i = 0; i < length; i++) {
    salt[i] = (uint8_t)(rand() % 256);
  }
}

char *hash_password(const char *password) {
  uint8_t hash[HASHLEN];
  uint8_t salt[SALTLEN];
  generate_salt(salt, SALTLEN);
  uint32_t pwdlen = (uint32_t)strlen(password);
  uint8_t *pwd = (uint8_t *)strdup(password);
  int rc = argon2id_hash_raw(T_COST, M_COST, PARALLELISM, pwd, pwdlen, salt,
                             SALTLEN, hash, HASHLEN);
  free(pwd);
  if (rc != ARGON2_OK) {
    LOG("Error hashing password: %s", argon2_error_message(rc));
    return NULL;
  }
  char *hash_hex = (char *)malloc(HASHLEN * 2 + SALTLEN * 2 + 1);
  for (int i = 0; i < SALTLEN; ++i) {
    sprintf(&hash_hex[i * 2], "%02x", salt[i]);
  }
  for (int i = 0; i < HASHLEN; ++i) {
    sprintf(&hash_hex[SALTLEN * 2 + i * 2], "%02x", hash[i]);
  }
  hash_hex[SALTLEN * 2 + HASHLEN * 2] = '\0';
  LOG("Hashed password: %s", hash_hex);
  return hash_hex;
}

int verify_password(const char *password, const char *hashed_password) {
  LOG("Verifying password against hashed password");
  uint8_t hash[HASHLEN];
  uint8_t salt[SALTLEN];
  for (int i = 0; i < SALTLEN; ++i) {
    sscanf(&hashed_password[i * 2], "%2hhx", &salt[i]);
  }
  uint32_t pwdlen = (uint32_t)strlen(password);
  uint8_t *pwd = (uint8_t *)strdup(password);
  int rc = argon2id_hash_raw(T_COST, M_COST, PARALLELISM, pwd, pwdlen, salt,
                             SALTLEN, hash, HASHLEN);
  free(pwd);
  if (rc != ARGON2_OK) {
    LOG("Error verifying password: %s", argon2_error_message(rc));
    return -1;
  }
  char hash_hex[HASHLEN * 2 + 1];
  for (int i = 0; i < HASHLEN; ++i) {
    sprintf(&hash_hex[i * 2], "%02x", hash[i]);
  }
  hash_hex[HASHLEN * 2] = '\0';
  LOG("Computed hash for verification: %s", hash_hex);
  return strcmp(hashed_password + SALTLEN * 2, hash_hex) == 0;
}

void test_password() {
  const char *password = "password";
  LOG("Testing password: %s", password);
  char *hashed_password = hash_password(password);
  if (hashed_password) {
    LOG("Hashed Password: %s\n", hashed_password);
    if (verify_password("password", hashed_password)) {
      LOG("Password is valid!\n");
    } else {
      LOG("Invalid password!\n");
    }
    free(hashed_password);
  } else {
    LOG("Failed to hash password");
  }
}
