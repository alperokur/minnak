#ifndef SECURITY_AUTHENTICATION_H
#define SECURITY_AUTHENTICATION_H

char *create_jwt(const int id, const char *username);
char *parse_jwt_payload(const char *jwt);
int is_jwt_expired(const char *jwt);
int validate_jwt(const char *jwt);
int test_jwt();

#endif // SECURITY_AUTHENTICATION_H
