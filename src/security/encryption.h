#ifndef SECURITY_ENCRYPTION_H
#define SECURITY_ENCRYPTION_H

char *hash_password(const char *password);
int verify_password(const char *password, const char *hashed_password);
void test_password();

#endif // SECURITY_ENCRYPTION_H
