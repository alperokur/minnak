#include "user.h"
#include "../util/log.h"
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_USERNAME_LENGTH 24
#define MAX_NAME_LENGTH 32
#define MIN_NAME_LENGTH 3
#define MIN_PASSWORD_LENGTH 8
#define MAX_PASSWORD_LENGTH 24
#define PHONE_MIN_LENGTH 12
#define PHONE_MAX_LENGTH 14

void free_user(User *user) {
  if (user) {
    LOG("Freeing user: %s", user->username);
    free(user->username);
    free(user->password);
    if (user->email) {
      LOG("Freeing user email: %s", user->email);
      free(user->email);
    }
    if (user->phone) {
      LOG("Freeing user phone: %s", user->phone);
      free(user->phone);
    }
    if (user->first_name) {
      LOG("Freeing user first name: %s", user->first_name);
      free(user->first_name);
    }
    if (user->last_name) {
      LOG("Freeing user last name: %s", user->last_name);
      free(user->last_name);
    }
    if (user->birth_date) {
      LOG("Freeing user birth date: %s", user->birth_date);
      free(user->birth_date);
    }
    free(user);
  }
}

const char *validate_length(const char *field_name, size_t length,
                            size_t min_length, size_t max_length) {
  if (length < min_length || length > max_length) {
    static char error_message[100];
    snprintf(error_message, sizeof(error_message),
             "{\"error\":\"%s must be between %zu and %zu characters long\"}",
             field_name, min_length, max_length);
    return error_message;
  }
  return NULL;
}

const char *validate_capitalization(const char *name) {
  char *name_copy = strdup(name);
  if (name_copy == NULL) {
    return "{\"error\":\"Memory allocation failed\"}";
  }

  const char *token;
  char *saveptr;
  token = strtok_r(name_copy, " ", &saveptr);
  while (token != NULL) {
    if (!isupper(token[0])) {
      free(name_copy);
      return "{\"error\":\"Each word must start with an uppercase letter\"}";
    }
    token = strtok_r(NULL, " ", &saveptr);
  }

  free(name_copy);
  return NULL;
}

const char *validate_characters(const char *name, const char *valid_chars) {
  for (size_t i = 0; i < strlen(name); i++) {
    if (!isalpha(name[i]) && !strchr(valid_chars, name[i])) {
      return "{\"error\":\"Field can only contain letters and specified "
             "characters\"}";
    }
  }
  return NULL;
}

const char *validate_username(const char *username) {
  LOG("Validating username: %s", username);
  size_t username_length = strlen(username);

  const char *length_error =
      validate_length("Username", username_length, 3, MAX_USERNAME_LENGTH);
  if (length_error)
    return length_error;

  if (isdigit(username[0])) {
    LOG("Username starts with a digit: %s", username);
    return "{\"error\":\"Username cannot start with a digit\"}";
  }

  const char *char_error = validate_characters(username, "._-");
  if (char_error)
    return char_error;

  LOG("Username validation passed");
  return NULL;
}

const char *validate_password(const char *password) {
  LOG("Validating password");
  size_t password_length = strlen(password);

  const char *length_error = validate_length(
      "Password", password_length, MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH);
  if (length_error)
    return length_error;

  int has_upper = 0;
  int has_lower = 0;
  int has_digit = 0;
  int has_special = 0;

  for (size_t i = 0; i < password_length; i++) {
    char c = password[i];
    if (isupper(c))
      has_upper = 1;
    else if (islower(c))
      has_lower = 1;
    else if (isdigit(c))
      has_digit = 1;
    else if (ispunct(c))
      has_special = 1;
  }

  if (!has_upper)
    return "{\"error\":\"Password must contain at least one uppercase "
           "letter\"}";
  if (!has_lower)
    return "{\"error\":\"Password must contain at least one lowercase "
           "letter\"}";
  if (!has_digit)
    return "{\"error\":\"Password must contain at least one digit\"}";
  if (!has_special)
    return "{\"error\":\"Password must contain at least one special "
           "character\"}";

  LOG("Password validation passed");
  return NULL;
}

const char *validate_email(const char *email) {
  LOG("Validating email: %s", email);
  const char *at_sign = strchr(email, '@');
  if (at_sign == NULL || at_sign == email || *(at_sign + 1) == '\0') {
    LOG("Invalid email format");
    return "{\"error\":\"Invalid email format\"}";
  }
  LOG("Email validation passed");
  return NULL;
}

const char *validate_phone(const char *phone) {
  LOG("Validating phone: %s", phone);
  size_t phone_length = strlen(phone);

  const char *length_error = validate_length(
      "Phone number", phone_length, PHONE_MIN_LENGTH, PHONE_MAX_LENGTH);
  if (length_error)
    return length_error;

  const char *local_number = phone + phone_length - 10;
  const char *country_code = phone;
  size_t country_code_length = phone_length - 10;

  if (country_code_length > 0 && country_code[0] == '+') {
    country_code_length--;
    country_code++;
  }

  for (size_t i = 0; i < country_code_length; i++) {
    if (!isdigit(country_code[i])) {
      LOG("Invalid character in country code: %c", country_code[i]);
      return "{\"error\":\"Country code can only contain digits\"}";
    }
  }

  for (size_t i = 0; i < 10; i++) {
    if (!isdigit(local_number[i])) {
      LOG("Invalid character in local number: %c", local_number[i]);
      return "{\"error\":\"Local number can only contain digits\"}";
    }
  }

  LOG("Phone validation passed");
  return NULL;
}

const char *validate_first_name(const char *first_name) {
  LOG("Validating first name: %s", first_name);
  size_t length = strlen(first_name);

  const char *length_error =
      validate_length("First name", length, MIN_NAME_LENGTH, MAX_NAME_LENGTH);
  if (length_error)
    return length_error;

  const char *capitalization_error = validate_capitalization(first_name);
  if (capitalization_error)
    return capitalization_error;

  const char *char_error = validate_characters(first_name, " ");
  if (char_error)
    return char_error;

  LOG("First name validation passed");
  return NULL;
}

const char *validate_last_name(const char *last_name) {
  LOG("Validating last name: %s", last_name);
  size_t length = strlen(last_name);

  const char *length_error =
      validate_length("Last name", length, MIN_NAME_LENGTH, MAX_NAME_LENGTH);
  if (length_error)
    return length_error;

  const char *capitalization_error = validate_capitalization(last_name);
  if (capitalization_error)
    return capitalization_error;

  const char *char_error = validate_characters(last_name, " ");
  if (char_error)
    return char_error;

  LOG("Last name validation passed");
  return NULL;
}

const char *validate_birth_date(const char *birth_date) {
  LOG("Validating birth date: %s", birth_date);

  if (strlen(birth_date) != 10 || birth_date[4] != '-' ||
      birth_date[7] != '-') {
    LOG("Invalid birth date format");
    return "{\"error\":\"Birth date must be in the format YYYY-MM-DD\"}";
  }

  for (size_t i = 0; i < strlen(birth_date); i++) {
    if ((i == 4 || i == 7) && birth_date[i] != '-') {
      LOG("Invalid character in birth date: %c", birth_date[i]);
      return "{\"error\":\"Birth date must be in the format YYYY-MM-DD\"}";
    }
    if (i != 4 && i != 7 && !isdigit(birth_date[i])) {
      LOG("Invalid character in birth date: %c", birth_date[i]);
      return "{\"error\":\"Birth date must be in the format YYYY-MM-DD\"}";
    }
  }

  LOG("Birth date validation passed");
  return NULL;
}
