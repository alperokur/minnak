#ifndef MODEL_USER_H
#define MODEL_USER_H

typedef struct {
  int id;
  char *username;
  char *password;
  char *email;
  char *phone;
  char *first_name;
  char *last_name;
  char *birth_date;
} User;

void free_user(User *user);

const char *validate_username(const char *username);
const char *validate_password(const char *password);
const char *validate_email(const char *email);
const char *validate_phone(const char *phone);
const char *validate_first_name(const char *first_name);
const char *validate_last_name(const char *last_name);
const char *validate_birth_date(const char *birth_date);

#endif // MODEL_USER_H
