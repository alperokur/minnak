#ifndef SERVICE_DATABASE_H
#define SERVICE_DATABASE_H

#include "../model/user.h"

void db_open_connection();
void db_close_connection();

void db_tables();

int db_create_user(const char *username, const char *password,
                   const char *email, const char *phone, const char *first_name,
                   const char *last_name, const char *birth_date);
User *db_find_user(const char *unique_column);

#endif // SERVICE_DATABASE_H
