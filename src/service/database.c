#include "database.h"
#include "../config/env.h"
#include "../security/encryption.h"
#include "../util/log.h"
#include <assert.h>
#include <libpq-fe.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PGconn *conn = NULL;

static void safe_exit() {
  LOG("Exiting safely, closing database connection if open");
  if (conn != NULL) {
    PQfinish(conn);
  }
  exit(1);
}

void db_open_connection() {
  LOG("Opening database connection");
  const char *db_host = env("DB_HOST");
  const char *db_port = env("DB_PORT");
  const char *db_user = env("DB_USER");
  const char *db_pass = env("DB_PASS");
  const char *db_name = env("DB_NAME");

  char conninfo[256];
  snprintf(conninfo, sizeof(conninfo),
           "host=%s port=%s user=%s password=%s dbname=%s", db_host, db_port,
           db_user, db_pass, db_name);

  conn = PQconnectdb(conninfo);
  if (PQstatus(conn) != CONNECTION_OK) {
    fprintf(stderr, "Connection error: %s", PQerrorMessage(conn));
    safe_exit();
  }
  LOG("Database connection established");
}

void db_close_connection() {
  LOG("Closing database connection");
  if (conn != NULL) {
    PQfinish(conn);
    conn = NULL;
  }
}

static void check_and_create_table(const char *query,
                                   const char *create_query) {
  PGresult *check_res = PQexec(conn, query);
  if (PQresultStatus(check_res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "Table check error: %s\n", PQerrorMessage(conn));
    PQclear(check_res);
    safe_exit();
  }

  if (strcmp(PQgetvalue(check_res, 0, 0), "f") == 0) {
    PGresult *create_res = PQexec(conn, create_query);
    if (PQresultStatus(create_res) != PGRES_COMMAND_OK) {
      fprintf(stderr, "Table creation error: %s\n", PQerrorMessage(conn));
      PQclear(create_res);
      PQclear(check_res);
      safe_exit();
    }
    PQclear(create_res);
  }

  PQclear(check_res);
}

void db_tables() {
  LOG("Checking and creating necessary tables");

  check_and_create_table(
      "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name "
      "= 'users');",
      "CREATE TABLE users (id SERIAL PRIMARY KEY, username VARCHAR(24) UNIQUE "
      "NOT NULL, password TEXT NOT NULL);");

  const char *columns[][2] = {{"email", "VARCHAR(128) UNIQUE"},
                              {"phone", "VARCHAR(14) UNIQUE"},
                              {"first_name", "VARCHAR(32)"},
                              {"last_name", "VARCHAR(24)"},
                              {"birth_date", "DATE"}};

  for (size_t i = 0; i < sizeof(columns) / sizeof(columns[0]); i++) {
    const char *column_name = columns[i][0];
    const char *column_type = columns[i][1];

    char check_column_query[256];
    snprintf(check_column_query, sizeof(check_column_query),
             "SELECT EXISTS (SELECT 1 FROM information_schema.columns WHERE "
             "table_name = 'users' AND column_name = '%s');",
             column_name);
    PGresult *check_column_res = PQexec(conn, check_column_query);
    if (PQresultStatus(check_column_res) != PGRES_TUPLES_OK) {
      fprintf(stderr, "%s column check error: %s\n", column_name,
              PQerrorMessage(conn));
      PQclear(check_column_res);
      safe_exit();
    }

    if (strcmp(PQgetvalue(check_column_res, 0, 0), "f") == 0) {
      LOG("Adding '%s' column to 'users' table", column_name);
      char alter_column_query[256];
      snprintf(alter_column_query, sizeof(alter_column_query),
               "ALTER TABLE users ADD COLUMN %s %s;", column_name, column_type);
      PGresult *alter_column_res = PQexec(conn, alter_column_query);
      if (PQresultStatus(alter_column_res) != PGRES_COMMAND_OK) {
        fprintf(stderr, "%s column addition error: %s\n", column_name,
                PQerrorMessage(conn));
        PQclear(alter_column_res);
        PQclear(check_column_res);
        safe_exit();
      }
      PQclear(alter_column_res);
    }

    PQclear(check_column_res);
  }

  LOG("Database tables checked and updated if necessary");
}

int db_create_user(const char *username, const char *password,
                   const char *email, const char *phone, const char *first_name,
                   const char *last_name, const char *birth_date) {
  LOG("Creating user: %s", username);
  char *hashed_password = hash_password(password);
  if (!hashed_password) {
    fprintf(stderr, "Password hashing failed\n");
    return -1;
  }

  const char *paramValues[7] = {username,   hashed_password, email,     phone,
                                first_name, last_name,       birth_date};

  if (PQsendQueryParams(conn,
                        "INSERT INTO users (username, password, email, phone, "
                        "first_name, last_name, birth_date) "
                        "VALUES ($1, $2, $3, $4, $5, $6, $7);",
                        7, NULL, paramValues, NULL, NULL, 0) == 0) {
    fprintf(stderr, "Error sending query: %s", PQerrorMessage(conn));
    free(hashed_password);
    return -1;
  }

  PGresult *res;
  while ((res = PQgetResult(conn)) != NULL) {
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
      fprintf(stderr, "%s", PQerrorMessage(conn));
      PQclear(res);
      free(hashed_password);
      return -1;
    }
    PQclear(res);
  }

  LOG("User '%s' created successfully.\n", username);
  free(hashed_password);
  return 0;
}

User *db_find_user(const char *username) {
  LOG("Finding user: %s", username);
  User *user = NULL;
  const char *query;
  char id_str[20];
  const char *param;

  if (username[0] == '+') {
    query = "SELECT id, username, password, email, phone, first_name, "
            "last_name, birth_date FROM users WHERE username = $1 OR email = "
            "$1 OR phone = $1;";
    param = username;
  } else {
    char *endptr;
    long id = strtol(username, &endptr, 10);

    if (*endptr == '\0') {
      snprintf(id_str, sizeof(id_str), "%ld", id);
      query = "SELECT id, username, password, email, phone, first_name, "
              "last_name, birth_date FROM users WHERE id = $1;";
      param = id_str;
    } else {
      query = "SELECT id, username, password, email, phone, first_name, "
              "last_name, birth_date FROM users WHERE username = $1 OR email = "
              "$1 OR phone = $1;";
      param = username;
    }
  }

  PGresult *res = PQexecParams(conn, query, 1, NULL, &param, NULL, NULL, 0);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "Query failed: %s", PQerrorMessage(conn));
    PQclear(res);
    return NULL;
  }

  if (PQntuples(res) > 0) {
    user = malloc(sizeof(User));
    if (user == NULL) {
      fprintf(stderr, "Memory allocation failed\n");
      PQclear(res);
      return NULL;
    }
    user->id = atoi(PQgetvalue(res, 0, 0));
    user->username = strdup(PQgetvalue(res, 0, 1));
    user->password = strdup(PQgetvalue(res, 0, 2));
    user->email = PQgetisnull(res, 0, 3) ? NULL : strdup(PQgetvalue(res, 0, 3));
    user->phone = PQgetisnull(res, 0, 4) ? NULL : strdup(PQgetvalue(res, 0, 4));
    user->first_name =
        PQgetisnull(res, 0, 5) ? NULL : strdup(PQgetvalue(res, 0, 5));
    user->last_name =
        PQgetisnull(res, 0, 6) ? NULL : strdup(PQgetvalue(res, 0, 6));
    user->birth_date =
        PQgetisnull(res, 0, 7) ? NULL : strdup(PQgetvalue(res, 0, 7));
    LOG("User found: id=%d, username=%s", user->id, user->username);
  } else {
    LOG("No user found with the given identifier");
  }
  PQclear(res);
  return user;
}
