#include "api.h"
#include "../config/env.h"
#include "../controller/auth/sign_in.h"
#include "../controller/auth/sign_up.h"
#include "../controller/users.h"
#include "../helper/json.h"
#include "../util/log.h"
#include <arpa/inet.h>
#include <cjson/cJSON.h>
#include <errno.h>
#include <ifaddrs.h>
#include <limits.h>
#include <microhttpd.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAC_ADDRESS_LENGTH 18
#define IP_ADDRESS_LENGTH INET6_ADDRSTRLEN
#define MAX_URL_LENGTH 256

struct connection_info {
  char *post_data;
  size_t post_data_size;
};

static char *get_mac_address() {
  struct ifaddrs *ifaddr;
  static char mac_address[MAC_ADDRESS_LENGTH] = "";

  if (getifaddrs(&ifaddr) == -1) {
    perror("getifaddrs");
    return NULL;
  }

  for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_INET &&
        (ifa->ifa_flags & IFF_RUNNING) && !(ifa->ifa_flags & IFF_LOOPBACK)) {
      const unsigned char *mac = (const unsigned char *)ifa->ifa_addr->sa_data;
      snprintf(mac_address, sizeof(mac_address),
               "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3],
               mac[4], mac[5]);
      break;
    }
  }

  freeifaddrs(ifaddr);
  return (mac_address[0] != '\0') ? mac_address : NULL;
}

static void log_connection_info(struct MHD_Connection *connection) {
  char ip_str[IP_ADDRESS_LENGTH];
  const union MHD_ConnectionInfo *conn_info =
      MHD_get_connection_info(connection, MHD_CONNECTION_INFO_CLIENT_ADDRESS);

  if (conn_info) {
    if (conn_info->client_addr->sa_family == AF_INET) {
      const struct sockaddr_in *addr =
          (const struct sockaddr_in *)conn_info->client_addr;
      inet_ntop(AF_INET, &addr->sin_addr, ip_str, sizeof(ip_str));
    } else if (conn_info->client_addr->sa_family == AF_INET6) {
      const struct sockaddr_in6 *addr =
          (const struct sockaddr_in6 *)conn_info->client_addr;
      inet_ntop(AF_INET6, &addr->sin6_addr, ip_str, sizeof(ip_str));
    }
    LOG("Client IP: %s", ip_str);
  }

  const char *mac_address = get_mac_address();
  if (mac_address) {
    LOG("MAC Address: %s", mac_address);
  }

  const char *user_agent =
      MHD_lookup_connection_value(connection, MHD_HEADER_KIND, "User-Agent");
  if (user_agent) {
    LOG("User-Agent: %s", user_agent);
  }

  const char *referer =
      MHD_lookup_connection_value(connection, MHD_HEADER_KIND, "Referer");
  if (referer) {
    LOG("Referer: %s", referer);
  }

  const char *accept_language = MHD_lookup_connection_value(
      connection, MHD_HEADER_KIND, "Accept-Language");
  if (accept_language) {
    LOG("Accept-Language: %s", accept_language);
  }
}

static enum MHD_Result
handle_options_request(struct MHD_Connection *connection) {
  struct MHD_Response *response = create_json_response("");
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);
  return ret;
}

static enum MHD_Result
handle_post_request(struct MHD_Connection *connection,
                    const struct connection_info *con_info, const char *url) {
  if (strcmp(url, "/auth/sign-up") == 0) {
    return handle_post_auth_sign_up(connection, con_info->post_data);
  } else if (strcmp(url, "/auth/sign-in") == 0) {
    return handle_post_auth_sign_in(connection, con_info->post_data);
  }
  return MHD_NO;
}

static enum MHD_Result handle_get_request(struct MHD_Connection *connection,
                                          const char *url) {
  const char *username = url + 7;
  if (strlen(username) == 0) {
    const char *empty_username_msg = "{\"error\":\"Username is required\"}";
    LOG("%s", empty_username_msg);
    struct MHD_Response *response = create_json_response(empty_username_msg);
    int ret = MHD_queue_response(connection, MHD_HTTP_BAD_REQUEST, response);
    MHD_destroy_response(response);
    return ret;
  }
  return handle_get_user(connection, username);
}

static enum MHD_Result request_handler(void *cls, // NOSONAR
                                       struct MHD_Connection *connection,
                                       const char *url, const char *method,
                                       const char *version,
                                       const char *upload_data,
                                       long unsigned int *upload_data_size,
                                       void **con_cls) { // NOSONAR
  (void)cls;
  (void)version;

  struct connection_info *con_info = *con_cls;

  if (*con_cls == NULL) {
    con_info = malloc(sizeof(struct connection_info));
    if (con_info == NULL) {
      LOG("Failed to allocate memory for connection info");
      return MHD_NO;
    }
    con_info->post_data = NULL;
    con_info->post_data_size = 0;
    *con_cls = con_info;
    return MHD_YES;
  }

  LOG("Handling request: %s %s", method, url);
  log_connection_info(connection);

  if (strcmp(method, "OPTIONS") == 0) {
    return handle_options_request(connection);
  }

  if (strcmp(method, "POST") == 0) {
    if (*upload_data_size != 0) {
      LOG("receiving upload data, size: %lu", *upload_data_size);
      con_info->post_data =
          realloc(con_info->post_data,
                  con_info->post_data_size + *upload_data_size + 1);
      if (con_info->post_data == NULL) {
        LOG("failed to allocate memory for post data");
        free(con_info);
        return MHD_NO;
      }
      memcpy(con_info->post_data + con_info->post_data_size, upload_data,
             *upload_data_size);
      con_info->post_data_size += *upload_data_size;
      con_info->post_data[con_info->post_data_size] = '\0';
      *upload_data_size = 0;
      return MHD_YES;
    } else if (con_info->post_data != NULL) {
      LOG("finalizing post request for %s", url);
      int ret = handle_post_request(connection, con_info, url);
      free(con_info->post_data);
      free(con_info);
      *con_cls = NULL;
      return ret;
    }
  }

  if (strcmp(method, "GET") == 0 && strncmp(url, "/users/", 7) == 0) {
    int ret = handle_get_request(connection, url);
    free(con_info);
    return ret;
  }

  LOG("No handler found for method: %s, url: %s", method, url);
  struct MHD_Response *response =
      MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);
  free(con_info);
  return ret;
}

int api(void) {
  struct MHD_Daemon *daemon;
  const char *port_str = env("API_PORT");
  if (port_str == NULL) {
    LOG("API_PORT environment variable is not set.");
    fprintf(stderr, "API_PORT environment variable is not set.\n");
    return 1;
  }

  char *endptr;
  long port_long = strtol(port_str, &endptr, 10);
  if (endptr == port_str || errno == ERANGE || port_long < 0 ||
      port_long > UINT16_MAX) {
    LOG("API_PORT is invalid or out of range.");
    fprintf(stderr, "API_PORT is invalid or out of range.\n");
    return 1;
  }

  uint16_t port = (uint16_t)port_long;

  daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD, port, NULL, NULL,
                            &request_handler, NULL, MHD_OPTION_END);
  if (daemon == NULL) {
    LOG("Failed to start MHD daemon");
    return 1;
  }
  printf("Starting server on http://localhost:%d/\n", port);
  getchar();
  MHD_stop_daemon(daemon);
  LOG("Server stopped");
  return 0;
}
