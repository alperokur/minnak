#include "sign_up.h"
#include "../../helper/json.h"
#include "../../model/user.h"
#include "../../service/database.h"
#include "../../util/log.h"
#include <cjson/cJSON.h>
#include <stdlib.h>
#include <string.h>

void free_auth_sign_up_user(AuthSignUpUser *user) {
  if (user) {
    LOG("Freeing AuthSignUpUser: username=%s", user->username);
    free(user->username);
    free(user->password);
    if (user->email)
      free(user->email);
    if (user->phone)
      free(user->phone);
    if (user->first_name)
      free(user->first_name);
    if (user->last_name)
      free(user->last_name);
    if (user->birth_date)
      free(user->birth_date);
    free(user);
  }
}

static int send_response(struct MHD_Connection *connection, const char *msg,
                         int http_code, int is_error) {
  if (is_error) {
    LOG("Sending error response: %s", msg);
  } else {
    LOG("Sending response: %s", msg);
  }

  struct MHD_Response *response = create_json_response(msg);
  int ret = MHD_queue_response(connection, http_code, response);
  MHD_destroy_response(response);
  return ret;
}

AuthSignUpUser *handle_post_auth_sign_up_user(const char *json_str) {
  AuthSignUpUser *user = malloc(sizeof(AuthSignUpUser));
  if (!user) {
    LOG("Failed to allocate memory for AuthSignUpUser");
    return NULL;
  }

  cJSON *jobj = cJSON_Parse(json_str);
  if (!jobj) {
    LOG("Failed to parse JSON");
    free(user);
    return NULL;
  }

  user->username =
      strdup(cJSON_GetStringValue(cJSON_GetObjectItem(jobj, "username")));
  user->password =
      strdup(cJSON_GetStringValue(cJSON_GetObjectItem(jobj, "password")));

  cJSON *email_item = cJSON_GetObjectItem(jobj, "email");
  cJSON *phone_item = cJSON_GetObjectItem(jobj, "phone");
  cJSON *first_name_item = cJSON_GetObjectItem(jobj, "first_name");
  cJSON *last_name_item = cJSON_GetObjectItem(jobj, "last_name");
  cJSON *birth_date_item = cJSON_GetObjectItem(jobj, "birth_date");

  user->email = email_item ? strdup(cJSON_GetStringValue(email_item)) : NULL;
  user->phone = phone_item ? strdup(cJSON_GetStringValue(phone_item)) : NULL;
  user->first_name =
      first_name_item ? strdup(cJSON_GetStringValue(first_name_item)) : NULL;
  user->last_name =
      last_name_item ? strdup(cJSON_GetStringValue(last_name_item)) : NULL;
  user->birth_date =
      birth_date_item ? strdup(cJSON_GetStringValue(birth_date_item)) : NULL;

  cJSON_Delete(jobj);

  if (!user->username || !user->password) {
    LOG("Username or password is NULL");
    free_auth_sign_up_user(user);
    return NULL;
  }

  LOG("Parsed user: username=%s, email=%s, phone=%s, first_name=%s, "
      "last_name=%s, birth_date=%s",
      user->username, user->email ? user->email : "NULL",
      user->phone ? user->phone : "NULL",
      user->first_name ? user->first_name : "NULL",
      user->last_name ? user->last_name : "NULL",
      user->birth_date ? user->birth_date : "NULL");

  return user;
}

static int validate_user_fields(const AuthSignUpUser *user,
                                struct MHD_Connection *connection) {
  const char *username_error = validate_username(user->username);
  if (username_error) {
    LOG("Username validation error: %s", username_error);
    return send_response(connection, username_error, MHD_HTTP_BAD_REQUEST, 1);
  }

  const char *password_error = validate_password(user->password);
  if (password_error) {
    LOG("Password validation error: %s", password_error);
    return send_response(connection, password_error, MHD_HTTP_BAD_REQUEST, 1);
  }

  if (user->email) {
    const char *email_error = validate_email(user->email);
    if (email_error) {
      LOG("Email validation error: %s", email_error);
      return send_response(connection, email_error, MHD_HTTP_BAD_REQUEST, 1);
    }
  }

  if (user->phone) {
    const char *phone_error = validate_phone(user->phone);
    if (phone_error) {
      LOG("Phone validation error: %s", phone_error);
      return send_response(connection, phone_error, MHD_HTTP_BAD_REQUEST, 1);
    }
  }

  if (user->first_name) {
    const char *first_name_error = validate_first_name(user->first_name);
    if (first_name_error) {
      LOG("First name validation error: %s", first_name_error);
      return send_response(connection, first_name_error, MHD_HTTP_BAD_REQUEST,
                           1);
    }
  }

  if (user->last_name) {
    const char *last_name_error = validate_last_name(user->last_name);
    if (last_name_error) {
      LOG("Last name validation error: %s", last_name_error);
      return send_response(connection, last_name_error, MHD_HTTP_BAD_REQUEST,
                           1);
    }
  }

  if (user->birth_date) {
    const char *birth_date_error = validate_birth_date(user->birth_date);
    if (birth_date_error) {
      LOG("Birth date validation error: %s", birth_date_error);
      return send_response(connection, birth_date_error, MHD_HTTP_BAD_REQUEST,
                           1);
    }
  }

  return 0;
}

int handle_post_auth_sign_up(struct MHD_Connection *connection,
                             const char *data) {
  AuthSignUpUser *user = handle_post_auth_sign_up_user(data);
  if (!user) {
    return send_response(connection,
                         "{\"error\":\"Failed to parse user from JSON\"}",
                         MHD_HTTP_BAD_REQUEST, 1);
  }

  int validation_result = validate_user_fields(user, connection);
  if (validation_result != 0) {
    free_auth_sign_up_user(user);
    return validation_result;
  }

  User *existing_user = db_find_user(user->username);
  if (existing_user) {
    LOG("User already exists: %s", user->username);
    free_user(existing_user);
    free_auth_sign_up_user(user);
    return send_response(connection, "{\"error\":\"User already exists\"}",
                         MHD_HTTP_CONFLICT, 1);
  }

  if (user->email) {
    User *existing_email_user = db_find_user(user->email);
    if (existing_email_user) {
      LOG("Email already exists: %s", user->email);
      free_user(existing_email_user);
      free_auth_sign_up_user(user);
      return send_response(connection, "{\"error\":\"Email already exists\"}",
                           MHD_HTTP_CONFLICT, 1);
    }
  }

  if (user->phone) {
    User *existing_phone_user = db_find_user(user->phone);
    if (existing_phone_user) {
      LOG("Phone number already exists: %s", user->phone);
      free_user(existing_phone_user);
      free_auth_sign_up_user(user);
      return send_response(connection,
                           "{\"error\":\"Phone number already exists\"}",
                           MHD_HTTP_CONFLICT, 1);
    }
  }

  if (db_create_user(user->username, user->password, user->email, user->phone,
                     user->first_name, user->last_name,
                     user->birth_date) == 0) {
    LOG("User created successfully: %s", user->username);
    const char *success_msg = "{\"message\":\"User created successfully\"}";
    free_auth_sign_up_user(user);
    return send_response(connection, success_msg, MHD_HTTP_CREATED, 0);
  } else {
    LOG("Failed to create user: %s", user->username);
    free_auth_sign_up_user(user);
    return send_response(connection, "{\"error\":\"Failed to create user\"}",
                         MHD_HTTP_INTERNAL_SERVER_ERROR, 1);
  }
}
