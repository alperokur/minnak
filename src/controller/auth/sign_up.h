#ifndef CONTROLLER_AUTH_SIGN_UP_H
#define CONTROLLER_AUTH_SIGN_UP_H

#include <microhttpd.h>

typedef struct {
  char *username;
  char *password;
  char *email;
  char *phone;
  char *first_name;
  char *last_name;
  char *birth_date;
} AuthSignUpUser;

void free_auth_sign_up_user(AuthSignUpUser *user);

AuthSignUpUser *handle_post_auth_sign_up_user(const char *json_str);

int handle_post_auth_sign_up(struct MHD_Connection *connection,
                             const char *data);

#endif // CONTROLLER_AUTH_SIGN_UP_H
