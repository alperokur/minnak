#include "sign_in.h"
#include "../../helper/json.h"
#include "../../model/user.h"
#include "../../security/authentication.h"
#include "../../security/encryption.h"
#include "../../service/database.h"
#include "../../util/log.h"
#include <cjson/cJSON.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void free_auth_sign_in_user(AuthSignInUser *user) {
  LOG("Freeing AuthSignInUser: username=%s", user->username);
  free(user->username);
  free(user->password);
  free(user);
}

AuthSignInUser *handle_post_auth_sign_in_user(const char *json_str) {
  AuthSignInUser *user = malloc(sizeof(AuthSignInUser));
  if (!user) {
    LOG("Failed to allocate memory for AuthSignInUser");
    return NULL;
  }

  cJSON *jobj = cJSON_Parse(json_str);
  if (!jobj) {
    free(user);
    return NULL;
  }

  const cJSON *jusername = cJSON_GetObjectItem(jobj, "username");
  const cJSON *jpassword = cJSON_GetObjectItem(jobj, "password");

  user->username = (jusername && jusername->valuestring)
                       ? strdup(jusername->valuestring)
                       : NULL;
  user->password = (jpassword && jpassword->valuestring)
                       ? strdup(jpassword->valuestring)
                       : NULL;

  cJSON_Delete(jobj);
  return user;
}

int handle_post_auth_sign_in(struct MHD_Connection *connection,
                             const char *data) {
  AuthSignInUser *user = handle_post_auth_sign_in_user(data);
  if (!user) {
    const char *parse_error_msg =
        "{\"error\":\"Failed to parse user from JSON\"}";
    struct MHD_Response *response = create_json_response(parse_error_msg);
    int ret = MHD_queue_response(connection, MHD_HTTP_BAD_REQUEST, response);
    MHD_destroy_response(response);
    return ret;
  }

  User *existing_user = db_find_user(user->username);
  if (!existing_user) {
    LOG("User %s not found in database", user->username);
  } else {
    LOG("User %s found in database", user->username);
  }

  if (!existing_user ||
      !verify_password(user->password, existing_user->password)) {
    const char *invalid_credentials_msg =
        "{\"error\":\"Invalid username or password\"}";
    LOG("%s", invalid_credentials_msg);
    struct MHD_Response *response =
        create_json_response(invalid_credentials_msg);
    free_auth_sign_in_user(user);
    if (existing_user) {
      LOG("Freeing existing user data for %s", existing_user->username);
      free_user(existing_user);
    }
    int ret = MHD_queue_response(connection, MHD_HTTP_UNAUTHORIZED, response);
    MHD_destroy_response(response);
    return ret;
  }

  LOG("Password verification successful for user %s", user->username);

  char *token = create_jwt(existing_user->id, existing_user->username);
  LOG("Created JWT token for user %s: %s", existing_user->username, token);

  char success_msg[512];
  snprintf(success_msg, sizeof(success_msg), "{\"token\":\"%s\"}", token);
  LOG("Sending response: %s", success_msg);

  struct MHD_Response *response = create_json_response(success_msg);
  char cookie[512];
  snprintf(cookie, sizeof(cookie),
           "Set-Cookie: token=%s; HttpOnly; Path=/; SameSite=Strict", token);
  MHD_add_response_header(response, "Set-Cookie", cookie);

  free(token);
  free_user(existing_user);
  free_auth_sign_in_user(user);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);
  return ret;
}
