#ifndef CONTROLLER_AUTH_SIGN_IN_H
#define CONTROLLER_AUTH_SIGN_IN_H

#include <microhttpd.h>

typedef struct {
  char *username;
  char *password;
} AuthSignInUser;

void free_auth_sign_in_user(AuthSignInUser *user);

AuthSignInUser *handle_post_auth_sign_in_user(const char *json_str);

int handle_post_auth_sign_in(struct MHD_Connection *connection,
                             const char *data);

#endif // CONTROLLER_AUTH_SIGN_IN_H
