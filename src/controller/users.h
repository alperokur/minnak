#ifndef CONTROLLER_USERS_H
#define CONTROLLER_USERS_H

#include "../model/user.h"
#include <microhttpd.h>

char *get_user_json(const User *user);

int handle_get_user(struct MHD_Connection *connection, const char *username);

#endif // CONTROLLER_USERS_H
