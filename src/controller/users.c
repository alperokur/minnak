#include "users.h"
#include "../helper/json.h"
#include "../security/authentication.h"
#include "../service/database.h"
#include "../util/log.h"
#include <cjson/cJSON.h>
#include <stdlib.h>
#include <string.h>

static int queue_json_response(struct MHD_Connection *connection,
                               int status_code, const char *json_message) {
  LOG("Queueing JSON response with status code %d: %s", status_code,
      json_message);
  struct MHD_Response *response = create_json_response(json_message);
  int ret = MHD_queue_response(connection, status_code, response);
  MHD_destroy_response(response);
  return ret;
}

char *get_user_json(const User *user) {
  cJSON *json_user = cJSON_CreateObject();
  if (json_user == NULL) {
    LOG("Failed to create JSON object for user");
    return NULL;
  }

  cJSON_AddNumberToObject(json_user, "id", user->id);
  cJSON_AddStringToObject(json_user, "username", user->username);
  if (user->email) {
    cJSON_AddStringToObject(json_user, "email", user->email);
  }
  if (user->phone) {
    cJSON_AddStringToObject(json_user, "phone", user->phone);
  }
  if (user->first_name) {
    cJSON_AddStringToObject(json_user, "first_name", user->first_name);
  }
  if (user->last_name) {
    cJSON_AddStringToObject(json_user, "last_name", user->last_name);
  }
  if (user->birth_date) {
    cJSON_AddStringToObject(json_user, "birth_date", user->birth_date);
  }

  char *json_string = cJSON_PrintUnformatted(json_user);
  cJSON_Delete(json_user);
  return json_string;
}

int handle_get_user(struct MHD_Connection *connection, const char *username) {
  const char *authorization_header =
      MHD_lookup_connection_value(connection, MHD_HEADER_KIND, "Authorization");

  if (authorization_header == NULL ||
      strncmp(authorization_header, "Bearer ", 7) != 0) {
    LOG("Authorization header missing or invalid");
    return queue_json_response(
        connection, MHD_HTTP_UNAUTHORIZED,
        "{\"error\":\"Authorization header missing or invalid\"}");
  }

  const char *token = authorization_header + 7;
  LOG("Validating token: %s", token);

  if (!validate_jwt(token)) {
    LOG("Invalid token");
    return queue_json_response(connection, MHD_HTTP_UNAUTHORIZED,
                               "{\"error\":\"Invalid token\"}");
  }

  User *user = db_find_user(username);
  if (user != NULL) {
    char *json_output = get_user_json(user);
    if (json_output != NULL) {
      int ret = queue_json_response(connection, MHD_HTTP_OK, json_output);
      free(json_output);
      free_user(user);
      return ret;
    }
    LOG("Failed to create JSON output for user");
    free_user(user);
  } else {
    LOG("User not found: %s", username);
  }

  return queue_json_response(connection, MHD_HTTP_NOT_FOUND,
                             "{\"error\":\"User not found\"}");
}
