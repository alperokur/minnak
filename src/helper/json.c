#include "json.h"
#include "../lib/string.h"
#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>

struct MHD_Response *create_json_response(const char *message) {
  size_t message_length = strlen(message);

  char *mutable_message = malloc(message_length + 1);
  if (mutable_message == NULL) {
    return NULL;
  }

  safe_strncpy(mutable_message, message, message_length,
               sizeof(mutable_message));

  struct MHD_Response *response = MHD_create_response_from_buffer(
      message_length, mutable_message, MHD_RESPMEM_MUST_FREE);

  MHD_add_response_header(response, "Content-Type", "application/json");
  MHD_add_response_header(response, "Access-Control-Allow-Origin", "*");
  MHD_add_response_header(response, "Access-Control-Allow-Methods",
                          "GET, POST, OPTIONS");
  MHD_add_response_header(response, "Access-Control-Allow-Headers",
                          "Content-Type");
  return response;
}
