#ifndef HELPER_JSON_H
#define HELPER_JSON_H

#include <microhttpd.h>

struct MHD_Response *create_json_response(const char *message);

#endif // HELPER_JSON_H
