#include "config/env.h"
#include "service/api.h"
#include "service/database.h"
#include <stdlib.h>
#include <time.h>

int main() {
  srand((unsigned int)time(NULL));
  load_env(".env");
  db_open_connection();
  db_tables();
  api();
  db_close_connection();
  free_env();
}
