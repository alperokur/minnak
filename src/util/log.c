#include "log.h"
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#define LOGDIR "log"

void log_message(const char *fmt, ...) { // NOSONAR
  time_t t = time(NULL);
  struct tm tm_info;
  localtime_r(&t, &tm_info);

  char filename[30];
  strftime(filename, sizeof(filename), LOGDIR "/%Y-%m-%d.log", &tm_info);

  if (mkdir(LOGDIR, 0700) == -1 && errno != EEXIST) {
    fprintf(stderr, "Could not create log directory: %s\nError: %s\n", LOGDIR,
            strerror(errno));
    return;
  }

  FILE *log_file = fopen(filename, "a");
  if (log_file) {
    char time_buffer[20];
    strftime(time_buffer, sizeof(time_buffer), "%Y-%m-%d %H:%M:%S", &tm_info);

    va_list args;
    va_start(args, fmt);
    fprintf(log_file, "%s: ", time_buffer);
    vfprintf(log_file, fmt, args); // NOSONAR
    fprintf(log_file, "\n");
    va_end(args);

    fclose(log_file);

#ifdef DEBUG
    fprintf(stdout, "\033[32m%s: ", time_buffer);
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    fprintf(stdout, "\033[0m\n");
    va_end(args);
#endif
  } else {
    fprintf(stderr, "Could not open log file: %s\nError: %s\n", filename,
            strerror(errno));
  }
}
