#ifndef UTIL_LOG_H
#define UTIL_LOG_H

void log_message(const char *fmt, ...);
#define LOG(fmt, ...) log_message(fmt, ##__VA_ARGS__)

#endif // UTIL_LOG_H
