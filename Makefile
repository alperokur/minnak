CC := gcc
CFLAGS_CMN := -I/usr/include/postgresql
CFLAGS_DBG := -Wall -Wextra -Werror -DDEBUG -g -O0
CFLAGS_RLS := -O3
LDFLAGS := -lpq -lcjson -lmicrohttpd -largon2 -lcrypto

V := valgrind
VARGS := --leak-check=full --track-origins=yes --show-leak-kinds=all -s

SRCDIR := src
BLDDIR := build
LOGDIR := log
DBGDIR := $(BLDDIR)/debug
RLSDIR := $(BLDDIR)/release
DBG_OBJDIR := $(DBGDIR)/obj
DBG_BINDIR := $(DBGDIR)/bin
RLS_OBJDIR := $(RLSDIR)/obj
RLS_BINDIR := $(RLSDIR)/bin

SRCS := $(shell find $(SRCDIR) -name "*.c")
DBG_OBJS := $(patsubst $(SRCDIR)/%.c, $(DBG_OBJDIR)/%.o, $(SRCS))
RLS_OBJS := $(patsubst $(SRCDIR)/%.c, $(RLS_OBJDIR)/%.o, $(SRCS))

MKDIR := mkdir -p
RMDIR := rm -rf
RSYNC := rsync -avq --include '*/' --exclude '*'

TARGET := $(notdir $(shell pwd))
DBG_TARGET := $(DBG_BINDIR)/$(TARGET)
RLS_TARGET := $(RLS_BINDIR)/$(TARGET)

.PHONY: all debug debug_build release release_build build clean

all: debug

$(DBG_OBJDIR)/%.o: $(SRCDIR)/%.c
	@$(MKDIR) $(DBG_OBJDIR)
	@$(RSYNC) $(SRCDIR)/ $(DBG_OBJDIR)
	$(CC) $(CFLAGS_CMN) $(CFLAGS_DBG) -c $< -o $@

debug_build: $(DBG_OBJS)
	@$(MKDIR) $(DBG_BINDIR)
	$(CC) $(CFLAGS_CMN) $(CFLAGS_DBG) $^ -o $(DBG_TARGET) $(LDFLAGS)

debug: debug_build
	./$(DBG_TARGET)

valgrind: debug_build
	@$(V) $(VARGS) ./$(DBG_TARGET)

gdb: debug_build
	@gdb --ex run ./$(DBG_TARGET)

$(RLS_OBJDIR)/%.o: $(SRCDIR)/%.c
	@$(MKDIR) $(RLS_OBJDIR)
	@$(RSYNC) $(SRCDIR)/ $(RLS_OBJDIR)
	@$(CC) $(CFLAGS_CMN) $(CFLAGS_RLS) $(CFLAGS_CMN) -c $< -o $@

release_build: $(RLS_OBJS)
	@$(MKDIR) $(RLS_BINDIR)
	@$(CC) $(CFLAGS_CMN) $(CFLAGS_RLS) $^ -o $(RLS_TARGET) $(LDFLAGS)

release: release_build
	@./$(RLS_TARGET)

build: debug_build release_build

clean:
	$(RMDIR) $(BLDDIR) $(LOGDIR)
