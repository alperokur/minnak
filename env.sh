#!/bin/bash

DEST_FILE=".env"
TEMP_FILE=$(mktemp)

JWT_SECRET=$(< /dev/urandom tr -dc 'A-Za-z0-9_!@#%^&*()-+=~' | head -c 32)

declare -A defaults=(
    ["DB_NAME"]="minnak"
    ["DB_USER"]="minnak"
    ["DB_PASS"]="minnak"
    ["DB_HOST"]="localhost"
    ["DB_PORT"]="5432"
    ["API_PORT"]="3000"
    ["JWT_EXPIRE"]="604800"
)

keys=(
    "DB_HOST"
    "DB_PORT"
    "DB_USER"
    "DB_PASS"
    "DB_NAME"
    "API_PORT"
)

if [[ "$1" == "-d" ]]; then
    for key in "${keys[@]}"; do
        echo "$key = \"${defaults[$key]}\"" >> "$TEMP_FILE"
    done
    echo "JWT_SECRET = \"$JWT_SECRET\"" >> "$TEMP_FILE"
    echo "JWT_EXPIRE = \"${defaults["JWT_EXPIRE"]}\"" >> "$TEMP_FILE"
else
    for key in "${keys[@]}"; do
        read -p "Enter $key (default: '${defaults[$key]}'): " input
        echo "$key = \"${input:-${defaults[$key]}}\"" >> "$TEMP_FILE"
    done

    read -p "Enter JWT_SECRET (default: random value): " input
    echo "JWT_SECRET = \"${input:-$JWT_SECRET}\"" >> "$TEMP_FILE"

    echo "JWT_EXPIRE = \"${input:-${defaults["JWT_EXPIRE"]}}\"" >> "$TEMP_FILE"
fi

mv "$TEMP_FILE" "$DEST_FILE"
echo ".env file has been successfully created and updated."
